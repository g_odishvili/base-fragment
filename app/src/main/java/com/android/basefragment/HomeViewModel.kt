package com.android.basefragment

import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {
    val list: List<String> by lazy { mutableListOf() }
}