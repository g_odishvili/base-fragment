package com.android.basefragment

import android.util.Log.i
import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.basefragment.databinding.HomeFragmentBinding

class HomeFragment : BaseFragment<HomeFragmentBinding, HomeViewModel>(
    HomeFragmentBinding::inflate,
    HomeViewModel::class.java
) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        binding.root.setOnClickListener {
            i("BLA", "BLU")
        }
        viewModel.list
    }
}